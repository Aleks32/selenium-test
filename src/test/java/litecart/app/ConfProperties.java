package litecart.app;

import java.io.FileInputStream;
import java.util.Properties;

public class ConfProperties {
    protected static Properties PROPERTIES = new Properties();

    static {
        try (FileInputStream fileInputStream = new FileInputStream("src/test/resources/conf.properties")) {
            PROPERTIES.load(fileInputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getProperty(String key) {
        return PROPERTIES.getProperty(key);
    }
}