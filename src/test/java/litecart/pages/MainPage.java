package litecart.pages;

import litecart.app.DriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.openqa.selenium.support.ui.ExpectedConditions.titleContains;

public class MainPage {
    public WebDriver driver;
    public WebDriverWait wait;

    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "//li[@class='product column shadow hover-light']")
    public List<WebElement> itemProduct;

    @FindBy(css = "div[id='box-campaigns'] li")
    public WebElement itemProductCampaigns;

    @FindBy(xpath = "//li/a[text()='Create Account']")
    public WebElement linkCreateAccount;

    @FindBy(xpath = "//div/ul[@class='list-vertical']/li/a[text()='Logout']")
    public WebElement linkLogout;

    @FindBy(css = "div[id='cart']")
    public WebElement cart;

    @FindBy(css = "div[id='box-account'] h3")
    public WebElement accountText;

    @FindBy(css = "div[id='box-account-login'] h3")
    public WebElement loginText;

    @FindBy(name = "email")
    private WebElement email;

    @FindBy(name = "password")
    private WebElement password;

    @FindBy(css = "button[name='login']")
    private WebElement btnLogin;




    public void clickByMenuItems() {
        for (int i = 0; i < itemProduct.size(); i++) {
            String itemName = itemProduct.get(i).findElement(By.cssSelector("div.name")).getText();
            String itemPrice = itemProduct.get(i).findElement(By.cssSelector("div.price-wrapper")).getText();
            int stickerCount = itemProduct.get(i).findElements(By.cssSelector("div.sticker")).size();
            Assert.assertEquals(String.format("У товара [%s] стоимостью [%s] некорректное количество стикеров - [%s]", itemName, itemPrice, stickerCount), 1, stickerCount);
        }
    }

    public Map<String, String> getNameItem() {
        Map<String, String> item = new HashMap<>();
        item.put("text", itemProductCampaigns.findElement(By.cssSelector("div.name")).getText());
        item.put("color", itemProductCampaigns.findElement(By.cssSelector("div.name")).getCssValue("color"));
        item.put("font-weight", itemProductCampaigns.findElement(By.cssSelector("div.name")).getCssValue("font-weight"));
        item.put("text-decoration", itemProductCampaigns.findElement(By.cssSelector("div.name")).getCssValue("text-decoration"));
        item.put("font-size", itemProductCampaigns.findElement(By.cssSelector("div.name")).getCssValue("font-size"));
        return item;
    }

    public Map<String, String> getPriceItem() {
        Map<String, String> item = new HashMap<>();
        item.put("text", itemProductCampaigns.findElement(By.cssSelector("div.price-wrapper s.regular-price")).getText());
        item.put("color", itemProductCampaigns.findElement(By.cssSelector("div.price-wrapper s.regular-price")).getCssValue("color"));
        item.put("font-weight", itemProductCampaigns.findElement(By.cssSelector("div.price-wrapper s.regular-price")).getCssValue("font-weight"));
        item.put("text-decoration", itemProductCampaigns.findElement(By.cssSelector("div.price-wrapper s.regular-price")).getCssValue("text-decoration"));
        item.put("font-size", itemProductCampaigns.findElement(By.cssSelector("div.price-wrapper s.regular-price")).getCssValue("font-size"));
        return item;
    }

    public Map<String, String> getPriceSaleItem() {
        Map<String, String> item = new HashMap<>();
        item.put("text", itemProductCampaigns.findElement(By.cssSelector("div.price-wrapper strong.campaign-price")).getText());
        item.put("color", itemProductCampaigns.findElement(By.cssSelector("div.price-wrapper strong.campaign-price")).getCssValue("color"));
        item.put("font-weight", itemProductCampaigns.findElement(By.cssSelector("div.price-wrapper strong.campaign-price")).getCssValue("font-weight"));
        item.put("text-decoration", itemProductCampaigns.findElement(By.cssSelector("div.price-wrapper strong.campaign-price")).getCssValue("text-decoration"));
        item.put("font-size", itemProductCampaigns.findElement(By.cssSelector("div.price-wrapper strong.campaign-price")).getCssValue("font-size"));
        return item;
    }

    public void fillLoginForm(String eMail) {
        email.sendKeys(eMail + "@gmail.com");
        password.sendKeys(eMail);
    }

    public void clickLoginBtn() {
        btnLogin.click();
    }

    public void logout() {
        linkLogout.click();
    }

    public void checkoutCart() {
        wait = new WebDriverWait(driver, DriverManager.getDefaultTimeOut());
        cart.click();
        wait.until(titleContains("Checkout"));
    }

    public void checkoutMainPage() {
        wait = new WebDriverWait(driver, DriverManager.getDefaultTimeOut());
        driver.get("http://localhost/litecart/en/");
        wait.until(titleContains("Online Store"));
    }

    public String getNameProduct(int j) {
        return itemProduct.get(j).findElement(By.cssSelector("div.name")).getText();
    }

    public void clickOnItemProduct(int j) {
        itemProduct.get(j).click();
    }
}