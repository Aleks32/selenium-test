package litecart.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.stalenessOf;


public class Cart {
    public WebDriver driver;
    public WebDriverWait wait;

    public Cart(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(css = "div[id='order_confirmation-wrapper']")
    public WebElement orderConfirmationWrapper;

    @FindBy(css = "li[class='shortcut']")
    public List<WebElement> shortcut;

    @FindBy(name = "remove_cart_item")
    public WebElement removeItem;

    public List<WebElement> getTableRows () {
        return orderConfirmationWrapper.findElements(By.cssSelector("table tr:not(.header,.footer)"));
    }

    public void removeItem () {
        getTableRows();
        removeItem.click();
    }
}
