package litecart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.Calendar;
import java.util.List;

public class RegisterPage {
    public WebDriver driver;

    public RegisterPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(name = "firstname")
    private WebElement firstname;

    @FindBy(name = "lastname")
    private WebElement lastname;

    @FindBy(name = "address1")
    private WebElement address;

    @FindBy(name = "postcode")
    private WebElement postcode;

    @FindBy(name = "city")
    private WebElement city;

    @FindBy(name = "email")
    private WebElement email;

    @FindBy(name = "phone")
    private WebElement phone;

    @FindBy(name = "password")
    private WebElement password;

    @FindBy(name = "confirmed_password")
    private WebElement confirmedPassword;

    @FindBy(name = "create_account")
    private WebElement createAccount;

    @FindBy(name = "country_code")
    private WebElement countryCode;

    @FindBy(css = "select[name='zone_code']")
    private WebElement zoneCode;

    @FindBy(xpath = "//ul[@class='select2-results__options']/li[@class='select2-results__option']")
    public List<WebElement> countries;

    @FindBy(css = "span[class='select2-selection__arrow']")
    public WebElement arrow;

    public void inputFirstname(String val) {
        new Actions(driver).click(firstname).perform();
        firstname.sendKeys(val);
    }

    public void inputLastname(String val) {
        lastname.sendKeys(val);
    }

    public void inputAddress(String val) {
        address.sendKeys(val);
    }

    public void inputPostcode(String val) {
        postcode.sendKeys(val);
    }

    public void inputCity(String val) {
        city.sendKeys(val);
    }

    public void inputEmail(String val) {
        email.sendKeys(val);
    }

    public void inputPhone(String val) {
        phone.sendKeys(val);
    }

    public void inputPassword(String val) {
        password.sendKeys(val);
    }

/*    public void selectCountry(String val) {
        Select country = new Select(countryCode);
        country.selectByVisibleText(val);
    }*/

    public void selectState(String val) {
        Select state = new Select(zoneCode);
        state.selectByVisibleText(val);
    }

    public void selectFromComboBoxByValue(List<WebElement> items, WebElement arrow, String val) {
        arrow.click();
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getText().contains(val)) {
                items.get(i).click();
            }
        }
    }

    public void inputConfirmedPassword(String val) {
        confirmedPassword.sendKeys(val);
    }

    public void clickOnCreateAccount() {
        createAccount.click();
    }

    public void fillRegisterForm(String eMail) {

        inputFirstname("Alexey");
        inputLastname("Erokhov");
        inputAddress("Moskovskaya st. 3");
        inputPostcode("24100");
        inputCity("Bryansk");
        selectFromComboBoxByValue(countries, arrow, "United States");
      //  selectCountry("United States");
        selectState("Alaska");
        inputEmail(eMail + "@gmail.com");
        inputPhone("9001234567");
        inputPassword(eMail);
        inputConfirmedPassword(eMail);

    }


}
