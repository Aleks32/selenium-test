package litecart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.HashMap;
import java.util.Map;

public class ProductPage {
    public WebDriver driver;

    public ProductPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(css = "h1.title")
    public WebElement itemNameProduct;

    @FindBy(css = ".campaign-price")
    public WebElement itemPriceSaleProduct;

    @FindBy(css = ".regular-price")
    public WebElement itemPriceProduct;

    @FindBy(css = "div[id='cart']")
    public WebElement cart;

    @FindBy(css = "span.quantity")
    public WebElement quantity;

    @FindBy(name = "options[Size]")
    public WebElement size;

    @FindBy(css = "button[name='cancel']")
    public WebElement cancel;

    @FindBy(name = "add_cart_product")
    public WebElement addCartProduct;

    public Map<String, String> getItemNameProduct() {
        Map<String, String> item = new HashMap<>();
        item.put("text", itemNameProduct.getText());
        item.put("color", itemNameProduct.getCssValue("color"));
        item.put("text-decoration", itemNameProduct.getCssValue("text-decoration"));
        item.put("font-size", itemNameProduct.getCssValue("font-size"));
        return item;
    }

    public Map<String, String> getItemPriceSaleProduct() {
        Map<String, String> item = new HashMap<>();
        item.put("text", itemPriceSaleProduct.getText());
        item.put("color", itemPriceSaleProduct.getCssValue("color"));
        item.put("font-weight", itemPriceSaleProduct.getTagName());
        item.put("text-decoration", itemPriceSaleProduct.getCssValue("text-decoration"));
        item.put("font-size", itemPriceSaleProduct.getCssValue("font-size"));
        return item;
    }

    public Map<String, String> getItemPriceProduct() {
        Map<String, String> item = new HashMap<>();
        item.put("text", itemPriceProduct.getText());
        item.put("color", itemPriceProduct.getCssValue("color"));
        item.put("text-decoration", itemPriceProduct.getCssValue("text-decoration"));
        item.put("font-size", itemPriceProduct.getCssValue("font-size"));
        return item;
    }

    public void clickOnAddCartProduct () {
        addCartProduct.click();
    }

    public void selectSize (int k, String text) {
        if (k == 0) {
            new Select(size).selectByVisibleText("Medium +$2.50");
        }
    }
}
