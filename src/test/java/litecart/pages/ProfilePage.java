package litecart.pages;

import litecart.app.DriverManager;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.io.File;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class ProfilePage {
    public WebDriver driver;
    public String originalWindow;
    public Set<String> existingWindows;

    public ProfilePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "//div[@id = 'box-apps-menu-wrapper']")
    public WebElement userMenu;

    @FindBy(xpath = "//li[@id = 'app-']")
    public List<WebElement> menuItem;

    @FindBy(xpath = ".//li[contains(@id, 'doc')]")
    public List<WebElement> subMenuItem;

    @FindBy(xpath = "//h1")
    public WebElement header;

    @FindBy(xpath = "//button[@name='cancel']")
    public WebElement cancelButton;

    @FindBy(xpath = "//table/tbody/tr[@class='row'][not(@class='header')]/td/a[text()]")
    public List<WebElement> nameCountries;

    @FindBy(xpath = "//table[@id='table-zones']//td[3][text()][not(@class='header')]|//td[3]/select/option[@selected='selected']")
    public List<WebElement> nameZones;

    @FindBy(css = "*[class='fa fa-external-link']")
    public List<WebElement> listLinks;

    @FindBy(xpath = "(//span[text()='Catalog'])[1]")
    public WebElement catalog;

    public List<String> getNameProductsInCatalog(List<WebElement> values) {
        List<String> strings = new ArrayList<>();
        for (WebElement e : values) {
            strings.add(e.getText());
        }
        return strings;
    }

    @FindBy(xpath = "//table/tbody/tr[@class='row']/td[3]/a[text()]")
    public List<WebElement> nameProductsInCatalog;

    @FindBy(xpath = "//table/tbody/tr[@class='row']/td[3]/img[contains(@style, '32px')]/following-sibling::a[text()]")
    public List<WebElement> nameProductsInCategory;

    @FindBy(linkText = "Add New Product")
    public WebElement addNewProduct;

    @FindBy(xpath = "//label[text()=' Enabled']")
    public WebElement status;

    @FindBy(name = "name[en]")
    public WebElement name;

    @FindBy(name = "code")
    public WebElement code;

    @FindBy(xpath = "(//input[@name='categories[]'])[2]")
    public WebElement rubberDucks;

    @FindBy(xpath = "(//input[@name='product_groups[]'])[3]")
    public WebElement unisex;

    @FindBy(name = "quantity")
    public WebElement quantity;

    @FindBy(name = "date_valid_from")
    public WebElement dateValidFrom;

    @FindBy(name = "date_valid_to")
    public WebElement dateValidTo;

    @FindBy(name = "new_images[]")
    public WebElement newImages;

    //Information
    @FindBy(linkText = "Information")
    public WebElement informationTab;

    @FindBy(name = "manufacturer_id")
    public WebElement manufacturer;

    @FindBy(name = "keywords")
    public WebElement keywords;

    @FindBy(name = "short_description[en]")
    public WebElement shortDescription;

    @FindBy(name = "description[en]")
    public WebElement description;

    @FindBy(name = "head_title[en]")
    public WebElement headTitle;

    @FindBy(name = "meta_description[en]")
    public WebElement metaDescription;

    //Prices
    @FindBy(linkText = "Prices")
    public WebElement pricesTab;

    @FindBy(name = "purchase_price")
    public WebElement purchasePrice;

    @FindBy(name = "purchase_price_currency_code")
    public WebElement purchasePriceCurrencyCode;

    @FindBy(name = "gross_prices[USD]")
    public WebElement grossPrices;

    @FindBy(name = "save")
    public WebElement save;

    public String getTextHeader() {
        return header.getText();
    }

    public void clickByMenuItems() {
        for (int i = 0; i < menuItem.size(); i++) {
            menuItem.get(i).click();
            int subMenuItemCount = subMenuItem.size();
            if (subMenuItemCount > 0) {
                for (int j = 0; j < subMenuItemCount; j++) {
                    subMenuItem.get(j).click();
                    Assert.assertTrue(String.format("Отсутствует заголовок для пункта подменю [%s]", subMenuItem.get(j).getText()), getTextHeader().length() > 0);
                }
            } else {
                Assert.assertTrue(String.format("Отсутствует заголовок для пункта меню [%s]", menuItem.get(i).getText()), getTextHeader().length() > 0);
            }
        }
    }

    public void checkCountriesZones(int page) {
        menuItem.get(page).click();
        sortByAsc(nameCountries);
        for (int i = 0; i < nameCountries.size(); i++) {
            int countZones;
            if (page == 2) {
                countZones = Integer.parseInt(nameCountries.get(i).findElement(By.xpath("./../../td[6]")).getText());
            } else {
                countZones = Integer.parseInt(nameCountries.get(i).findElement(By.xpath("./../../td[4]")).getText());
            }
            if (countZones > 0) {
                nameCountries.get(i).click();
                sortByAsc(nameZones);
                cancelButton.click();
            }
        }
    }

    public void sortByAsc(List<WebElement> list) {
        ArrayList<String> obtainedList = new ArrayList<>();
        for (WebElement we : list) {
            obtainedList.add(we.getText());
        }
        ArrayList<String> sortedList = new ArrayList<>(obtainedList);
        Collections.sort(sortedList);
        Assert.assertEquals(("Не отсортированы в алфавитном порядке"), sortedList, obtainedList);
    }

    public void fillGeneralTab() {
        Calendar calendar = Calendar.getInstance();
        int h = calendar.get(calendar.HOUR_OF_DAY);
        int m = calendar.get(calendar.MINUTE);
        int s = calendar.get(calendar.SECOND);

        int y = calendar.get(calendar.YEAR);
        int month = calendar.get(calendar.MONTH);
        int d = calendar.get(calendar.DAY_OF_MONTH);

        String nameprod = "Duck";
        String prefix = String.valueOf(h + m + s);
        String prodName = nameprod + " " + prefix;

        String validFrom = Integer.toString(y);
        String validTo = Integer.toString(y + 2);

        if (month < 10) {
            validFrom = validFrom + "-0" + month;
            validTo = validTo + "-0" + month;
        } else {
            validFrom = validFrom + "-" + month;
            validTo = validTo + "-" + month;
        }

        if (d < 10) {
            validFrom = validFrom + "-0" + d;
            validTo = validTo + "-0" + d;
        } else {
            validFrom = validFrom + "-" + d;
            validTo = validTo + "-" + d;
        }
        status.click();
        name.clear();
        name.sendKeys(prodName);
        code.sendKeys(prefix + Keys.TAB);
        rubberDucks.click();
        unisex.click();
        quantity.sendKeys("2");
        File file = new File("src/test/resources/duck.png");
        String absolutePath = file.getAbsolutePath();
        newImages.sendKeys(absolutePath);
        dateValidFrom.sendKeys(validFrom);
        dateValidTo.sendKeys(validTo);

    }

    public void fillInformationTab() {
        informationTab.click();
        String nameprod = "Duck";
        driver.manage().timeouts().pageLoadTimeout(DriverManager.getPageLoadTimeOut(), TimeUnit.SECONDS);

        new Select(manufacturer).selectByVisibleText("ACME Corp.");
        keywords.sendKeys(nameprod);
        shortDescription.sendKeys(nameprod);
        description.sendKeys(nameprod + nameprod);
        headTitle.sendKeys(nameprod);
        metaDescription.sendKeys("abcdefg");
    }

    public void fillPricesTab() {
        pricesTab.click();
        driver.manage().timeouts().pageLoadTimeout(DriverManager.getPageLoadTimeOut(), TimeUnit.SECONDS);
        purchasePrice.sendKeys("123");
        new Select(purchasePriceCurrencyCode).selectByVisibleText("Euros");
        grossPrices.sendKeys("321");
    }

    public void selectRandomCountry() {
        int countryCount = nameCountries.size();
        final Random random = new Random();
        int randomIndex = random.nextInt(countryCount - 1);
        nameCountries.get(randomIndex).click();
    }
}