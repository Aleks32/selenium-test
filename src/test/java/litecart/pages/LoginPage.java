package litecart.pages;

import litecart.app.ConfProperties;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    public WebDriver driver;

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "//div[@id='formLogin']")
    private WebElement formLogin;

    @FindBy(xpath = "//td[text()='Username']/..//input")
    private WebElement loginField;

    @FindBy(xpath = "//td[text()='Password']/..//input")
    private WebElement passwdField;

    @FindBy(xpath = "//div[contains(@class, 'footer')]/button")
    private WebElement loginBtn;

    public void inputLogin(String login) {
        loginField.sendKeys(login);
    }

    public void inputPasswd(String passwd) {
        passwdField.sendKeys(passwd);
    }

    public void clickLoginBtn() {
        loginBtn.click();
    }

    public void loginPageElements() {
        inputLogin(ConfProperties.getProperty("login"));
        inputPasswd(ConfProperties.getProperty("password"));
        clickLoginBtn();
    }
}