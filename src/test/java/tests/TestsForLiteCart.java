package tests;

import litecart.app.ConfProperties;
import litecart.app.DriverManager;
import litecart.pages.*;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;

public class TestsForLiteCart extends TestBase {

    private LoginPage loginPage;
    private ProfilePage profilePage;
    private MainPage mainPage;
    private ProductPage productPage;
    private RegisterPage registerPage;
    private Cart cart;

    @Test
    public void loginTest() {
        loginPage = new LoginPage(driver);
        profilePage = new ProfilePage(driver);
        driver.navigate().to(ConfProperties.getProperty("loginPage"));
        driver.manage().timeouts().implicitlyWait(DriverManager.getDefaultImplicitlyWaitTimeOut(), TimeUnit.SECONDS);
        loginPage.loginPageElements();
        driver.manage().timeouts().pageLoadTimeout(DriverManager.getPageLoadTimeOut(), TimeUnit.SECONDS);
        wait.until(ExpectedConditions.visibilityOf(profilePage.userMenu));
    }

    @Test
    public void clickByMenuItemTest() {
        loginTest();
        profilePage.clickByMenuItems();
    }

    @Test
    public void checkStickersTest() {
        mainPage = new MainPage(driver);
        driver.navigate().to(ConfProperties.getProperty("mainPage"));
        driver.manage().timeouts().implicitlyWait(DriverManager.getDefaultImplicitlyWaitTimeOut(), TimeUnit.SECONDS);
        mainPage.clickByMenuItems();
    }

    @Test
    public void checkSortCountriesZones() {
        loginTest();
        profilePage.checkCountriesZones(2);
    }

    @Test
    public void checkSortCountriesZones2() {
        loginTest();
        profilePage.checkCountriesZones(5);
    }

    @Test
    public void checkAttributes() {
        mainPage = new MainPage(driver);
        productPage = new ProductPage(driver);
        driver.navigate().to(ConfProperties.getProperty("mainPage"));
        driver.manage().timeouts().implicitlyWait(DriverManager.getPageLoadTimeOut(), TimeUnit.SECONDS);
        Map<String, String> nameMain = mainPage.getNameItem();
        Map<String, String> priceMain = mainPage.getPriceItem();
        Map<String, String> priceSaleMain = mainPage.getPriceSaleItem();
        Assert.assertTrue(("Размер обычной цены больше акционной на главной странице"), Double.parseDouble(priceSaleMain.get("font-size").replace("px", "")) > Double.parseDouble(priceMain.get("font-size").replace("px", "")));
        Assert.assertEquals(("Цвет цены на главной странице не является серым"), priceMain.get("color"), "rgba(119, 119, 119, 1)");
        Assert.assertTrue(("Обычная цена не перечеркнута"), priceMain.get("text-decoration").contains("line-through"));
        Assert.assertEquals(("Цвет акционной цены на главной странице не является красным"), priceSaleMain.get("color"), "rgba(204, 0, 0, 1)");
        Assert.assertEquals(("Шрифт акционной цены нежирный"), priceSaleMain.get("font-weight"), "700");

        mainPage.itemProductCampaigns.click();
        driver.manage().timeouts().pageLoadTimeout(DriverManager.getPageLoadTimeOut(), TimeUnit.SECONDS);
        Assert.assertEquals(("Значение наименования товара не совпадает"), nameMain.get("text"), productPage.getItemNameProduct().get("text"));
        Assert.assertEquals(("Значение цены товара не совпадает"), priceMain.get("text"), productPage.getItemPriceProduct().get("text"));
        Assert.assertEquals(("Значение акционной цены товара не совпадает"), priceSaleMain.get("text"), productPage.getItemPriceSaleProduct().get("text"));

        Assert.assertTrue(("Размер обычной цены больше акционной на странице товара"), Double.parseDouble(productPage.getItemPriceSaleProduct().get("font-size").replace("px", "")) > Double.parseDouble(productPage.getItemPriceProduct().get("font-size").replace("px", "")));
        Assert.assertEquals(("Цвет цены на странице товара не является серым"), productPage.getItemPriceProduct().get("color"), "rgba(102, 102, 102, 1)");
        Assert.assertTrue(("Обычная цена на странице товара не перечеркнута"), productPage.getItemPriceProduct().get("text-decoration").contains("line-through"));
        Assert.assertEquals(("Цвет акционной цены на главной странице не является красным"), productPage.getItemPriceSaleProduct().get("color"), "rgba(204, 0, 0, 1)");
        Assert.assertEquals(("Шрифт акционной цены нежирный"), productPage.getItemPriceSaleProduct().get("font-weight"), "700");

    }

    @Test
    public void checkRegistrationAndLogin() {

        Calendar calendar = Calendar.getInstance();
        int h = calendar.get(calendar.HOUR_OF_DAY);
        int m = calendar.get(calendar.MINUTE);
        int s = calendar.get(calendar.SECOND);

        String eMail = "Alexey" + h + m + s;
        mainPage = new MainPage(driver);
        registerPage = new RegisterPage(driver);
        driver.manage().window().maximize();
        driver.navigate().to(ConfProperties.getProperty("mainPage"));
        driver.manage().timeouts().pageLoadTimeout(DriverManager.getDefaultImplicitlyWaitTimeOut(), TimeUnit.SECONDS);
        mainPage.linkCreateAccount.click();
        registerPage.fillRegisterForm(eMail);
        registerPage.clickOnCreateAccount();
        driver.manage().timeouts().pageLoadTimeout(DriverManager.getPageLoadTimeOut(), TimeUnit.SECONDS);
        Assert.assertEquals(("На главной странице отсутствует надпись Account"), "Account", mainPage.accountText.getText());

        mainPage.logout();
        driver.manage().timeouts().implicitlyWait(DriverManager.getDefaultImplicitlyWaitTimeOut(), TimeUnit.SECONDS);
        Assert.assertEquals(("На главной странице отсутствует надпись Login"), "Login", mainPage.loginText.getText());

        mainPage.fillLoginForm(eMail);
        mainPage.clickLoginBtn();
        driver.manage().timeouts().implicitlyWait(DriverManager.getDefaultImplicitlyWaitTimeOut(), TimeUnit.SECONDS);
        Assert.assertEquals(("На главной странице отсутствует надпись Account"), "Account", mainPage.accountText.getText());

        mainPage.logout();
        driver.manage().timeouts().pageLoadTimeout(DriverManager.getPageLoadTimeOut(), TimeUnit.SECONDS);
        Assert.assertEquals(("На главной странице отсутствует надпись Login"), "Login", mainPage.loginText.getText());
    }

    @Test
    public void addNewProduct() {
        loginTest();
        profilePage.catalog.click();
        profilePage.addNewProduct.click();
        driver.manage().timeouts().implicitlyWait(DriverManager.getDefaultImplicitlyWaitTimeOut(), TimeUnit.SECONDS);
        profilePage.fillGeneralTab();
        String nameProduct = profilePage.name.getText();
        profilePage.fillInformationTab();
        profilePage.fillPricesTab();
        profilePage.save.click();
        driver.manage().timeouts().implicitlyWait(DriverManager.getDefaultImplicitlyWaitTimeOut(), TimeUnit.SECONDS);
        Assert.assertTrue(("Товар не содержится в каталоге"), profilePage.getNameProductsInCatalog(profilePage.nameProductsInCatalog).stream().anyMatch(item -> item.contains(nameProduct)));
    }

    @Test
    public void testNewWindows() {
        profilePage = new ProfilePage(driver);
        String originalWindow;
        Set<String> existingWindows;
        loginTest();
        wait.until(titleIs("My Store"));
        profilePage.menuItem.get(2).click();
        wait.until(titleContains("Countries"));

        profilePage.selectRandomCountry();
        wait.until(titleContains("Edit Country"));
        for (int i = 0; i < profilePage.listLinks.size(); i++) {
            originalWindow = driver.getWindowHandle();
            existingWindows = driver.getWindowHandles();
            profilePage.listLinks.get(i).click();
            String newWindow = wait.until(anyWindowOtherThan(existingWindows));
            driver.switchTo().window(newWindow);
            driver.close();
            driver.switchTo().window(originalWindow);
        }

    }

    @Test
    public void checkCart() {
        int i, j, k, k1, p;
        String[] prodName = new String[3];
        mainPage = new MainPage(driver);
        productPage = new ProductPage(driver);
        cart = new Cart(driver);
        for (i = 0; i < 3; i++) {
            mainPage.checkoutMainPage();

            p = 1;
            j = 0;
            while (p > 0) {
                k = 1;
                k1 = 1;
                prodName[i] = mainPage.getNameProduct(j);

                if (i == 1) {
                    k = prodName[i].compareToIgnoreCase(prodName[i - 1]);
                }

                if (i == 2) {
                    k = prodName[i].compareToIgnoreCase(prodName[i - 1]);
                    k1 = prodName[i].compareToIgnoreCase(prodName[i - 2]);
                }

                if ((k == 0) || (k1 == 0)) {
                    j++;
                } else {
                    p = 0;
                }
            }

            mainPage.clickOnItemProduct(j);
            wait.until(titleContains(prodName[i]));

            wait.until(ExpectedConditions.visibilityOf(productPage.cart));
            k = prodName[i].compareToIgnoreCase("Yellow Duck");

            productPage.selectSize(k, "Medium +$2.50");
            productPage.clickOnAddCartProduct();
            wait.until(textToBePresentInElement(
                    productPage.quantity,
                    Integer.toString(i + 1)));
        }
        mainPage.checkoutMainPage();
        mainPage.checkoutCart();
        for (int n = 1; n <= 3; n++) {
            wait.until(ExpectedConditions.visibilityOf(cart.orderConfirmationWrapper));

            if (cart.shortcut.size() > 0) {
                cart.shortcut.get(0).click();
            }
            cart.removeItem();
            wait.until(stalenessOf(cart.getTableRows().get(i)));

        }
        mainPage.checkoutMainPage();
    }

    @Test
    public void getBrowserLogs() {
        loginTest();
        profilePage = new ProfilePage(driver);
        productPage = new ProductPage(driver);
        driver.get("http://localhost/litecart/admin/?app=catalog&doc=catalog&category_id=1");
        for (int i = 0; i < profilePage.nameProductsInCategory.size(); i++) {
            profilePage.nameProductsInCategory.get(i).click();
            productPage.cancel.click();
            driver.manage().logs().get("browser").getAll().forEach(System.out::println);
        }
    }
}
