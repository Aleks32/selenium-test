package litecart.app;

public class DriverManager {

    public static Long getPageLoadTimeOut(){
        return Long.parseLong(ConfProperties.getProperty("pageLoadTimeOut"));
    }

    public static Long getDefaultImplicitlyWaitTimeOut(){
        return Long.parseLong(ConfProperties.getProperty("implicitlyWait"));
    }

    public static Long getDefaultTimeOut(){
        return Long.parseLong(ConfProperties.getProperty("defaultTimeOut"));
    }
}
